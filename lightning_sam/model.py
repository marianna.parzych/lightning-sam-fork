import random
from typing import Tuple, Union

import torch
import torch.nn as nn
import torch.nn.functional as F
from segment_anything import SamPredictor, sam_model_registry
import numpy as np
from torchvision.utils import draw_keypoints, save_image, draw_bounding_boxes
import cv2


class Model(nn.Module):
    BOXES = "boxes"
    MASKS = "masks"
    POINTS = "points"

    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg
        self.image_counter = 0


    def setup(self):
        self.model = sam_model_registry[self.cfg.model.type](checkpoint=self.cfg.model.checkpoint)
        self.model.train()
        if self.cfg.model.freeze.image_encoder:
            for param in self.model.image_encoder.parameters():
                param.requires_grad = False
        if self.cfg.model.freeze.prompt_encoder:
            for param in self.model.prompt_encoder.parameters():
                param.requires_grad = False
        if self.cfg.model.freeze.mask_decoder:
            for param in self.model.mask_decoder.parameters():
                param.requires_grad = False
    
    def sample_points(self, mask, positive=True, img=None):
        # What if there are few blobs in the image?
        kernel = np.ones((3,3), np.uint8)
        if positive:
            mask = cv2.erode(mask, kernel, iterations=2)
        else:
            #img mask is used so that we do not sample points from padding
            img_mask = (img != 0).any(axis=0).cpu().numpy()
            mask = (np.logical_not(cv2.dilate(mask, kernel, iterations=3)) & img_mask)

        points = np.where(mask)
        point_id = np.random.randint(0, len(points[0]))

        return points[1][point_id], points[0][point_id]

    def prepare_prompts(
        self, bboxes: Tuple[torch.Tensor], masks: Tuple[torch.Tensor], images=None
    ) -> Tuple[Union[Tuple[torch.Tensor], None]]:
        if self.cfg.other.prompts == self.POINTS:
            device = bboxes[0].device
            points = []
          
            for mask, image  in zip(masks, images):
                mask = mask.sum(axis=0).cpu().numpy().astype(np.uint8)
                sampled_points = [self.sample_points(mask), self.sample_points(mask, positive=False, img=image)]
                points_labels = [1, -1]


                points_coords = torch.Tensor(sampled_points).unsqueeze(1).to(device)
                points_labels = torch.Tensor(points_labels).unsqueeze(1).to(device)

                points.append((points_coords, points_labels))
            bboxes = [None for p in points]
            masks = [None for p in points]

        elif self.cfg.other.prompts == self.BOXES:
            bboxes = bboxes
            points = [None for b in bboxes]
            masks = [None for b in bboxes]

        elif self.cfg.other.prompts == self.MASKS:
            if masks is None:
                raise ValueError("Can't perform mask-prompt based training," \
                    "if masks are not provided by dataloder."  \
                    "Choose different prompt type."
                )
            masks = [
                (
                    F.interpolate(
                        mask.unsqueeze(1),
                        (
                            256,
                            256,
                        ),  # SAM needs mask prompt in shape (BxNxHxW), where H=W=256
                        mode="bilinear",
                        align_corners=False,
                    )
                )
                for mask in masks
            ]
            points = [None for m in masks]
            bboxes = [None for m in masks]
            
        return bboxes, masks, points

    def forward(self, images, bboxes, masks = None):
        _, _, H, W = images.shape
        image_embeddings = self.model.image_encoder(images)
        pred_masks = []
        ious = []
        
        #original_bboxes = bboxes
        #original_masks = masks
        bboxes, masks, points = self.prepare_prompts(bboxes, masks, images)

        # for i in range(len(images)):
        #     #draw points on every image
        #     if points[i] is not None:
        #         image_with_points = draw_keypoints((images[i] * 255).to(torch.uint8), points[i][0], radius=10, colors="red")
        #         image_with_points = draw_bounding_boxes(image_with_points, original_bboxes[i], colors="green", width=5)
        #         masks_to_plot = [original_mask.repeat(3,1,1).cpu() for original_mask in original_masks[i]]
        #         save_image([*masks_to_plot, image_with_points.float() / 255] , f"/mnt/ml-team/segment-anything-research/sebastian/lit_evaluation/points_{self.image_counter}.png")
        #         self.image_counter += 1

        for embedding, bbox, mask, point in zip(image_embeddings, bboxes, masks, points):
            sparse_embeddings, dense_embeddings = self.model.prompt_encoder(
                points=point,
                boxes=bbox,
                masks=mask,
            )

            low_res_masks, iou_predictions = self.model.mask_decoder(
                image_embeddings=embedding.unsqueeze(0),
                image_pe=self.model.prompt_encoder.get_dense_pe(),
                sparse_prompt_embeddings=sparse_embeddings,
                dense_prompt_embeddings=dense_embeddings,
                multimask_output=False,
            )

            masks = F.interpolate(
                low_res_masks,
                (H, W),
                mode="bilinear",
                align_corners=False,
            )
            pred_masks.append(masks.squeeze(1))
            ious.append(iou_predictions)

        return pred_masks, ious

    def get_predictor(self):
        return SamPredictor(self.model)
