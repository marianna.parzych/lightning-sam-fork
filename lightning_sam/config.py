from box import Box

config = {
    "num_devices": 1,
    "batch_size": 2,
    "num_workers": 1,
    "num_epochs": 20,
    "eval_interval": 2,
    "out_dir": "out/training/",
    "opt": {
        "learning_rate": 8e-5,
        "weight_decay": 1e-4,
        "decay_factor": 10,
        "steps": [60000, 86666],
        "warmup_steps": 250,
    },
    "other": {
        "too_many_objects": 20,  # int | None
        "prompts": "points",  # "masks", "boxes"
        },
    "model": {
        "type": 'vit_b',  # 'vit_h', 'vit_l'
        "checkpoint": "/mnt/ml-team/segment-anything-research/model/sam_vit_b_01ec64.pth",
        # "checkpoint": "/mnt/ml-team/segment-anything-research/model/sam_vit_h_4b8939.pth",
        # "checkpoint": "/mnt/ml-team/segment-anything-research/model/sam_vit_l_0b3195.pth",
        "freeze": {
            "image_encoder": True,
            "prompt_encoder": True,
            "mask_decoder": False,
        },
    },
    "dataset": {
        "dataset_type": "instance_segm",  # "binary_segm", "coco_dataset"
        "dataset_config_file": "configs/datasets/umbrella_dataset.yaml",
        # "dataset_config_file": "configs/datasets/car_damage_dataset.yaml",
        # "dataset_config_file": "configs/datasets/kvasig_dataset.yaml",
        # "dataset_config_file": "configs/datasets/coco128_dataset.yaml",
    }
}

cfg = Box(config)
